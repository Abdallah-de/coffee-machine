import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JButton;
import javax.swing.ImageIcon;
import java.awt.Font;
import javax.swing.JLabel;
import javax.swing.SwingConstants;
import javax.swing.JTextField;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class Pmenu extends JFrame {

	private JPanel contentPane;
	private JTextField Americano;
	private JTextField txtCapp;
	private JTextField txtLatte;
	private JTextField txtMachhiato;
	private JTextField txtMocha;
	private JTextField txtWhoiteMohca;
	private JButton Size_1;
	private JButton Size_2;
	private JButton Size_3;
	private JButton Size_4;
	private JButton Size_5;
	private JButton Size_6;
	private JButton Size_7;
	private JButton Size_8;
	private JButton Size_9;
	private JButton Size_10;
	private JButton Size_11;
	private JTextField txtIcedCoffee;
	private JTextField txtIcedTea;
	private JTextField txtMochaLatte;
	private JTextField txtAppleJuice;
	private JButton btnNewButton;
	private JButton btnNewButton_1;
	private JButton btnNewButton_2;
	private JLabel lblNewLabel;
	private JButton btnNewButton_3;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Pmenu frame = new Pmenu();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	
	  //frame customization.
	 
	public Pmenu() {
		setResizable(false);
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 855, 716);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lbl1 = new JLabel("ESPRESSO");
		lbl1.setFont(new Font("Trebuchet MS", Font.BOLD, 25));
		lbl1.setHorizontalAlignment(SwingConstants.CENTER);
		lbl1.setBounds(-71, 11, 422, 50);
		contentPane.add(lbl1);
		
		Americano = new JTextField();
		Americano.setEditable(false);
		Americano.setFont(new Font("Trebuchet MS", Font.BOLD, 14));
		Americano.setHorizontalAlignment(SwingConstants.CENTER);
		Americano.setText("Americano ");
		Americano.setBounds(28, 72, 121, 32);
		contentPane.add(Americano);
		Americano.setColumns(10);
		
		txtCapp = new JTextField();
		txtCapp.setEditable(false);
		txtCapp.setText("Cappuccino");
		txtCapp.setHorizontalAlignment(SwingConstants.CENTER);
		txtCapp.setFont(new Font("Trebuchet MS", Font.BOLD, 14));
		txtCapp.setColumns(10);
		txtCapp.setBounds(28, 115, 121, 32);
		contentPane.add(txtCapp);
		
		txtLatte = new JTextField();
		txtLatte.setEditable(false);
		txtLatte.setText("Latte");
		txtLatte.setHorizontalAlignment(SwingConstants.CENTER);
		txtLatte.setFont(new Font("Trebuchet MS", Font.BOLD, 14));
		txtLatte.setColumns(10);
		txtLatte.setBounds(28, 164, 121, 32);
		contentPane.add(txtLatte);
		
		txtMachhiato = new JTextField();
		txtMachhiato.setEditable(false);
		txtMachhiato.setText("Machhiato ");
		txtMachhiato.setHorizontalAlignment(SwingConstants.CENTER);
		txtMachhiato.setFont(new Font("Trebuchet MS", Font.BOLD, 14));
		txtMachhiato.setColumns(10);
		txtMachhiato.setBounds(28, 207, 121, 32);
		contentPane.add(txtMachhiato);
		
		txtMocha = new JTextField();
		txtMocha.setEditable(false);
		txtMocha.setText("Mocha");
		txtMocha.setHorizontalAlignment(SwingConstants.CENTER);
		txtMocha.setFont(new Font("Trebuchet MS", Font.BOLD, 14));
		txtMocha.setColumns(10);
		txtMocha.setBounds(28, 250, 121, 32);
		contentPane.add(txtMocha);
		
		txtWhoiteMohca = new JTextField();
		txtWhoiteMohca.setEditable(false);
		txtWhoiteMohca.setText("Whoite Mohca");
		txtWhoiteMohca.setHorizontalAlignment(SwingConstants.CENTER);
		txtWhoiteMohca.setFont(new Font("Trebuchet MS", Font.BOLD, 14));
		txtWhoiteMohca.setColumns(10);
		txtWhoiteMohca.setBounds(28, 293, 121, 32);
		contentPane.add(txtWhoiteMohca);
		
		JButton Size = new JButton("3$");
		Size.setFont(new Font("Tahoma", Font.BOLD, 11));
		Size.setBounds(189, 73, 67, 32);
		contentPane.add(Size);
		
		JLabel lbl = new JLabel("Small ");
		lbl.setHorizontalAlignment(SwingConstants.CENTER);
		lbl.setFont(new Font("Tahoma", Font.BOLD, 12));
		lbl.setBounds(201, 54, 46, 14);
		contentPane.add(lbl);
		
		JLabel lbl2 = new JLabel("Large");
		lbl2.setHorizontalAlignment(SwingConstants.CENTER);
		lbl2.setFont(new Font("Tahoma", Font.BOLD, 12));
		lbl2.setBounds(286, 54, 46, 14);
		contentPane.add(lbl2);
		
		Size_1 = new JButton("5$");
		Size_1.setFont(new Font("Tahoma", Font.BOLD, 11));
		Size_1.setBounds(276, 72, 67, 32);
		contentPane.add(Size_1);
		
		Size_2 = new JButton("3$");
		Size_2.setFont(new Font("Tahoma", Font.BOLD, 11));
		Size_2.setBounds(189, 116, 67, 32);
		contentPane.add(Size_2);
		
		Size_3 = new JButton("5$");
		Size_3.setFont(new Font("Tahoma", Font.BOLD, 11));
		Size_3.setBounds(276, 116, 67, 32);
		contentPane.add(Size_3);
		
		Size_4 = new JButton("4.20$");
		Size_4.setFont(new Font("Tahoma", Font.BOLD, 11));
		Size_4.setBounds(189, 165, 67, 32);
		contentPane.add(Size_4);
		
		Size_5 = new JButton("7.30$");
		Size_5.setFont(new Font("Tahoma", Font.BOLD, 11));
		Size_5.setBounds(276, 165, 67, 32);
		contentPane.add(Size_5);
		
		Size_6 = new JButton("4.50$");
		Size_6.setFont(new Font("Tahoma", Font.BOLD, 11));
		Size_6.setBounds(189, 208, 67, 32);
		contentPane.add(Size_6);
		
		Size_7 = new JButton("9.20$");
		Size_7.setFont(new Font("Tahoma", Font.BOLD, 11));
		Size_7.setBounds(276, 208, 67, 32);
		contentPane.add(Size_7);
		
		Size_8 = new JButton("4.50$");
		Size_8.setFont(new Font("Tahoma", Font.BOLD, 11));
		Size_8.setBounds(189, 251, 67, 32);
		contentPane.add(Size_8);
		
		Size_9 = new JButton("8.20$");
		Size_9.setFont(new Font("Tahoma", Font.BOLD, 11));
		Size_9.setBounds(276, 251, 67, 32);
		contentPane.add(Size_9);
		
		Size_10 = new JButton("5$");
		Size_10.setFont(new Font("Tahoma", Font.BOLD, 11));
		Size_10.setBounds(189, 294, 67, 32);
		contentPane.add(Size_10);
		
		Size_11 = new JButton("10$");
		Size_11.setFont(new Font("Tahoma", Font.BOLD, 11));
		Size_11.setBounds(276, 294, 67, 32);
		contentPane.add(Size_11);
		
		JLabel lblIce = new JLabel("ICED");
		lblIce.setHorizontalAlignment(SwingConstants.CENTER);
		lblIce.setFont(new Font("Trebuchet MS", Font.BOLD, 25));
		lblIce.setBounds(427, 11, 422, 50);
		contentPane.add(lblIce);
		
		txtIcedCoffee = new JTextField();
		txtIcedCoffee.setText("Iced Coffee");
		txtIcedCoffee.setHorizontalAlignment(SwingConstants.CENTER);
		txtIcedCoffee.setFont(new Font("Trebuchet MS", Font.BOLD, 14));
		txtIcedCoffee.setEditable(false);
		txtIcedCoffee.setColumns(10);
		txtIcedCoffee.setBounds(471, 72, 121, 32);
		contentPane.add(txtIcedCoffee);
		
		txtIcedTea = new JTextField();
		txtIcedTea.setText("Iced Tea");
		txtIcedTea.setHorizontalAlignment(SwingConstants.CENTER);
		txtIcedTea.setFont(new Font("Trebuchet MS", Font.BOLD, 14));
		txtIcedTea.setEditable(false);
		txtIcedTea.setColumns(10);
		txtIcedTea.setBounds(471, 115, 121, 32);
		contentPane.add(txtIcedTea);
		
		JButton Size_20 = new JButton("8$");
		Size_20.setFont(new Font("Tahoma", Font.BOLD, 11));
		Size_20.setBounds(625, 73, 67, 32);
		contentPane.add(Size_20);
		
		JButton Size_21 = new JButton("7.50$");
		Size_21.setFont(new Font("Tahoma", Font.BOLD, 11));
		Size_21.setBounds(625, 116, 67, 32);
		contentPane.add(Size_21);
		
		JButton Size_22 = new JButton("12$");
		Size_22.setFont(new Font("Tahoma", Font.BOLD, 11));
		Size_22.setBounds(722, 73, 67, 32);
		contentPane.add(Size_22);
		
		JButton Size_23 = new JButton("11$");
		Size_23.setFont(new Font("Tahoma", Font.BOLD, 11));
		Size_23.setBounds(722, 116, 67, 32);
		contentPane.add(Size_23);
		
		txtMochaLatte = new JTextField();
		txtMochaLatte.setText("Mocha Latte ");
		txtMochaLatte.setHorizontalAlignment(SwingConstants.CENTER);
		txtMochaLatte.setFont(new Font("Trebuchet MS", Font.BOLD, 14));
		txtMochaLatte.setEditable(false);
		txtMochaLatte.setColumns(10);
		txtMochaLatte.setBounds(471, 164, 121, 32);
		contentPane.add(txtMochaLatte);
		
		txtAppleJuice = new JTextField();
		txtAppleJuice.setText("Apple Juice ");
		txtAppleJuice.setHorizontalAlignment(SwingConstants.CENTER);
		txtAppleJuice.setFont(new Font("Trebuchet MS", Font.BOLD, 14));
		txtAppleJuice.setEditable(false);
		txtAppleJuice.setColumns(10);
		txtAppleJuice.setBounds(471, 207, 121, 32);
		contentPane.add(txtAppleJuice);
		
		JButton Size_21_1 = new JButton("5.20$");
		Size_21_1.setFont(new Font("Tahoma", Font.BOLD, 11));
		Size_21_1.setBounds(625, 165, 67, 32);
		contentPane.add(Size_21_1);
		
		JButton Size_21_2 = new JButton("5$");
		Size_21_2.setFont(new Font("Tahoma", Font.BOLD, 11));
		Size_21_2.setBounds(625, 208, 67, 32);
		contentPane.add(Size_21_2);
		
		JButton Size_21_3 = new JButton("10$");
		Size_21_3.setFont(new Font("Tahoma", Font.BOLD, 11));
		Size_21_3.setBounds(722, 165, 67, 32);
		contentPane.add(Size_21_3);
		
		JButton Size_21_4 = new JButton("8$");
		Size_21_4.setFont(new Font("Tahoma", Font.BOLD, 11));
		Size_21_4.setBounds(722, 208, 67, 32);
		contentPane.add(Size_21_4);
		
		JLabel lbl_2 = new JLabel("Small ");
		lbl_2.setHorizontalAlignment(SwingConstants.CENTER);
		lbl_2.setFont(new Font("Tahoma", Font.BOLD, 12));
		lbl_2.setBounds(640, 54, 46, 14);
		contentPane.add(lbl_2);
		
		JLabel lbl2_2 = new JLabel("Large");
		lbl2_2.setHorizontalAlignment(SwingConstants.CENTER);
		lbl2_2.setFont(new Font("Tahoma", Font.BOLD, 12));
		lbl2_2.setBounds(736, 54, 46, 14);
		contentPane.add(lbl2_2);
		
		btnNewButton = new JButton("Back");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				ChooseB cb = new ChooseB();
				cb.setVisible(true);
				dispose();
			}
		});
		btnNewButton.setFont(new Font("Trebuchet MS", Font.BOLD, 28));
		btnNewButton.setBounds(300, 595, 233, 66);
		contentPane.add(btnNewButton);
		
		btnNewButton_1 = new JButton("sweet ");
		btnNewButton_1.setFont(new Font("Trebuchet MS", Font.BOLD, 16));
		btnNewButton_1.setBounds(451, 434, 141, 32);
		contentPane.add(btnNewButton_1);
		
		btnNewButton_2 = new JButton("orginal ");
		btnNewButton_2.setFont(new Font("Trebuchet MS", Font.BOLD, 16));
		btnNewButton_2.setBounds(252, 434, 141, 32);
		contentPane.add(btnNewButton_2);
		
		lblNewLabel = new JLabel("chooose your suger");
		lblNewLabel.setFont(new Font("Trebuchet MS", Font.BOLD, 36));
		lblNewLabel.setHorizontalAlignment(SwingConstants.CENTER);
		lblNewLabel.setBounds(163, 358, 520, 60);
		contentPane.add(lblNewLabel);
		
		btnNewButton_3 = new JButton("Receipt");
		btnNewButton_3.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Rece r = new Rece();
				r.setVisible(true);
			}
		});
		btnNewButton_3.setFont(new Font("Trebuchet MS", Font.BOLD, 28));
		btnNewButton_3.setBounds(300, 508, 233, 66);
		contentPane.add(btnNewButton_3);
	}
}
