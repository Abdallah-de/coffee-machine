import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.SwingConstants;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class Mmenu extends JFrame {

	private JPanel contentPane;
	private JTextField textField;
	private JTextField textField_1;
	private JTextField textField_2;
	private JTextField textField_3;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Mmenu frame = new Mmenu();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Mmenu() {
		setResizable(false);
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 639, 582);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblTea = new JLabel("TEA");
		lblTea.setHorizontalAlignment(SwingConstants.CENTER);
		lblTea.setFont(new Font("Trebuchet MS", Font.BOLD, 25));
		lblTea.setBounds(90, 0, 422, 50);
		contentPane.add(lblTea);
		
		textField = new JTextField();
		textField.setText("Earl Grey");
		textField.setHorizontalAlignment(SwingConstants.CENTER);
		textField.setFont(new Font("Trebuchet MS", Font.BOLD, 14));
		textField.setEditable(false);
		textField.setColumns(10);
		textField.setBounds(151, 61, 121, 32);
		contentPane.add(textField);
		
		textField_1 = new JTextField();
		textField_1.setText("Green Tea");
		textField_1.setHorizontalAlignment(SwingConstants.CENTER);
		textField_1.setFont(new Font("Trebuchet MS", Font.BOLD, 14));
		textField_1.setEditable(false);
		textField_1.setColumns(10);
		textField_1.setBounds(151, 104, 121, 32);
		contentPane.add(textField_1);
		
		textField_2 = new JTextField();
		textField_2.setText("Jasmine Tea");
		textField_2.setHorizontalAlignment(SwingConstants.CENTER);
		textField_2.setFont(new Font("Trebuchet MS", Font.BOLD, 14));
		textField_2.setEditable(false);
		textField_2.setColumns(10);
		textField_2.setBounds(151, 147, 121, 32);
		contentPane.add(textField_2);
		
		textField_3 = new JTextField();
		textField_3.setText("English Breakfast");
		textField_3.setHorizontalAlignment(SwingConstants.CENTER);
		textField_3.setFont(new Font("Trebuchet MS", Font.BOLD, 14));
		textField_3.setEditable(false);
		textField_3.setColumns(10);
		textField_3.setBounds(151, 190, 121, 32);
		contentPane.add(textField_3);
		
		JButton Size_12 = new JButton("3.75$");
		Size_12.setFont(new Font("Tahoma", Font.BOLD, 11));
		Size_12.setBounds(300, 61, 67, 32);
		contentPane.add(Size_12);
		
		JButton Size_13 = new JButton("6.20$");
		Size_13.setFont(new Font("Tahoma", Font.BOLD, 11));
		Size_13.setBounds(377, 61, 67, 32);
		contentPane.add(Size_13);
		
		JButton Size_14 = new JButton("2$");
		Size_14.setFont(new Font("Tahoma", Font.BOLD, 11));
		Size_14.setBounds(300, 105, 67, 32);
		contentPane.add(Size_14);
		
		JButton Size_15 = new JButton("3.50$");
		Size_15.setFont(new Font("Tahoma", Font.BOLD, 11));
		Size_15.setBounds(377, 105, 67, 32);
		contentPane.add(Size_15);
		
		JButton Size_16 = new JButton("4.50$");
		Size_16.setFont(new Font("Tahoma", Font.BOLD, 11));
		Size_16.setBounds(300, 148, 67, 32);
		contentPane.add(Size_16);
		
		JButton Size_17 = new JButton("8$");
		Size_17.setFont(new Font("Tahoma", Font.BOLD, 11));
		Size_17.setBounds(377, 148, 67, 32);
		contentPane.add(Size_17);
		
		JButton Size_18 = new JButton("4.20$");
		Size_18.setFont(new Font("Tahoma", Font.BOLD, 11));
		Size_18.setBounds(300, 191, 67, 32);
		contentPane.add(Size_18);
		
		JButton Size_19 = new JButton("7.50$");
		Size_19.setFont(new Font("Tahoma", Font.BOLD, 11));
		Size_19.setBounds(377, 191, 67, 32);
		contentPane.add(Size_19);
		
		JLabel lbl_1 = new JLabel("Small ");
		lbl_1.setHorizontalAlignment(SwingConstants.CENTER);
		lbl_1.setFont(new Font("Tahoma", Font.BOLD, 12));
		lbl_1.setBounds(311, 46, 46, 14);
		contentPane.add(lbl_1);
		
		JLabel lbl2_1 = new JLabel("Large");
		lbl2_1.setHorizontalAlignment(SwingConstants.CENTER);
		lbl2_1.setFont(new Font("Tahoma", Font.BOLD, 12));
		lbl2_1.setBounds(381, 46, 46, 14);
		contentPane.add(lbl2_1);
		
		JButton btnNewButton = new JButton("Back");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				ChooseB cb2 = new ChooseB();
				cb2.setVisible(true);
				dispose();
			}
		});
		btnNewButton.setFont(new Font("Trebuchet MS", Font.BOLD, 25));
		btnNewButton.setBounds(209, 441, 200, 65);
		contentPane.add(btnNewButton);
		
		JButton btnNewButton_3 = new JButton("Receipt");
		btnNewButton_3.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Rece r = new Rece();
				r.setVisible(true);
			}
		});
		btnNewButton_3.setFont(new Font("Trebuchet MS", Font.BOLD, 25));
		btnNewButton_3.setBounds(209, 355, 200, 66);
		contentPane.add(btnNewButton_3);
		
		JLabel lblNewLabel = new JLabel("chooose your suger");
		lblNewLabel.setHorizontalAlignment(SwingConstants.CENTER);
		lblNewLabel.setFont(new Font("Trebuchet MS", Font.BOLD, 30));
		lblNewLabel.setBounds(51, 233, 520, 60);
		contentPane.add(lblNewLabel);
		
		JButton btnNewButton_2 = new JButton("orginal ");
		btnNewButton_2.setFont(new Font("Trebuchet MS", Font.BOLD, 16));
		btnNewButton_2.setBounds(153, 291, 141, 32);
		contentPane.add(btnNewButton_2);
		
		JButton btnNewButton_1 = new JButton("sweet ");
		btnNewButton_1.setFont(new Font("Trebuchet MS", Font.BOLD, 16));
		btnNewButton_1.setBounds(332, 293, 141, 32);
		contentPane.add(btnNewButton_1);
	}
}
