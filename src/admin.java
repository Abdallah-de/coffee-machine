import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.JPasswordField;
import javax.swing.JButton;
import java.awt.Color;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class admin extends JFrame {

	private JPanel contentPane;
	private JTextField user;
	private JPasswordField pw;
	private JLabel lbl_user;
	private JLabel lbl_user_1;
	

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					admin frame = new admin();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public admin() {
		setTitle("Admin System Login");
		setResizable(false);
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblNewLabel = new JLabel("Username");
		lblNewLabel.setHorizontalAlignment(SwingConstants.CENTER);
		lblNewLabel.setFont(new Font("Trebuchet MS", Font.BOLD, 12));
		lblNewLabel.setBounds(36, 82, 67, 14);
		contentPane.add(lblNewLabel);
		
		user = new JTextField();
		user.setFont(new Font("Tahoma", Font.BOLD, 11));
		user.setColumns(10);
		user.setBounds(113, 74, 247, 33);
		contentPane.add(user);
		
		pw = new JPasswordField();
		pw.setFont(new Font("Tahoma", Font.BOLD, 11));
		pw.setBounds(113, 128, 247, 33);
		contentPane.add(pw);
		
		JLabel lblPassword = new JLabel("Password");
		lblPassword.setFont(new Font("Trebuchet MS", Font.BOLD, 12));
		lblPassword.setBounds(46, 133, 52, 20);
		contentPane.add(lblPassword);
		
		JButton LoginButton = new JButton("Login");	
		LoginButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				if (user.getText().equals("admin") && pw.getText().equals("adminpw")) {
					history h = new history();
					h.setVisible(true);
					dispose();
				}
				else if (user.getText().equals("") && pw.getText().equals("") ) {
					lbl_user.setText("username is empty!!"); 
					lbl_user_1.setText("password is empty!!");
					
				}
				else if (user.getText().equals("")) {
					lbl_user.setText("Username is empty!!"); 
				}
				else if (pw.getText().equals("")) {
					lbl_user_1.setText("Password is empty!!");
				}
				else {
					wrong w = new wrong();
					w.setVisible(true);
					
				}
			}
			});
		
		
		
		LoginButton.setFont(new Font("Tahoma", Font.BOLD, 13));
		LoginButton.setBackground(Color.LIGHT_GRAY);
		LoginButton.setBounds(173, 191, 89, 23);
		contentPane.add(LoginButton);
		
		lbl_user = new JLabel("");
		lbl_user.setFont(new Font("Trebuchet MS", Font.BOLD, 10));
		lbl_user.setForeground(Color.RED);
		lbl_user.setBounds(113, 108, 179, 20);
		contentPane.add(lbl_user);
		
		lbl_user_1 = new JLabel("");
		lbl_user_1.setFont(new Font("Trebuchet MS", Font.BOLD, 10));
		lbl_user_1.setForeground(Color.RED);
		lbl_user_1.setBounds(113, 160, 179, 20);
		contentPane.add(lbl_user_1);
	}
}
