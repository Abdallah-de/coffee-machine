import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.SwingConstants;
import java.awt.Font;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Color;

public class ChooseB extends JFrame {

	private JPanel contentPane;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					ChooseB frame = new ChooseB();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public ChooseB() {
		setResizable(false);
		setBackground(Color.WHITE);
		setTitle("Coffee Machine ");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 862, 406);
		contentPane = new JPanel();
		contentPane.setBackground(Color.WHITE);
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblNewLabel = new JLabel("BigBlock - Coffee Vending Machine");
		lblNewLabel.setFont(new Font("Trebuchet MS", Font.BOLD, 30));
		lblNewLabel.setHorizontalAlignment(SwingConstants.CENTER);
		lblNewLabel.setBounds(118, 11, 572, 76);
		contentPane.add(lblNewLabel);
		
		JButton PlainC = new JButton("COFFEE");
		PlainC.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Pmenu coffee = new Pmenu();
				coffee.setVisible(true);
				dispose();
				
			
				
			}
		});
		PlainC.setFont(new Font("Trebuchet MS", Font.BOLD, 14));
		PlainC.setBounds(187, 241, 168, 64);
		contentPane.add(PlainC);
		
		JLabel CoffeType = new JLabel("Please choose drink type ");
		CoffeType.setFont(new Font("Trebuchet MS", Font.BOLD, 18));
		CoffeType.setHorizontalAlignment(SwingConstants.CENTER);
		CoffeType.setBounds(135, 177, 549, 64);
		contentPane.add(CoffeType);
		
		JButton MixedCoffe = new JButton("TEA");
		MixedCoffe.addActionListener(new ActionListener() { 
			public void actionPerformed(ActionEvent e) {
				Mmenu tea = new Mmenu();
				tea.setVisible(true);
				dispose();
			}
		});
		
		MixedCoffe.setFont(new Font("Trebuchet MS", Font.BOLD, 15));
		MixedCoffe.setBounds(474, 240, 168, 64);
		contentPane.add(MixedCoffe);
		
		JButton btnNewButton = new JButton("Admin");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				admin ad = new admin();
				ad.setVisible(true);
				dispose();
			}
		});
		btnNewButton.setBounds(771, 343, 75, 23);
		contentPane.add(btnNewButton);
	}

}
