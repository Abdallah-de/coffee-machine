import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.SwingConstants;
import java.awt.Font;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class CorT extends JFrame {

	private JPanel contentPane;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					CorT frame = new CorT();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public CorT() {
		setResizable(false);
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 506, 200);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblNewLabel = new JLabel("What do you want to buy?");
		lblNewLabel.setFont(new Font("Trebuchet MS", Font.BOLD, 18));
		lblNewLabel.setHorizontalAlignment(SwingConstants.CENTER);
		lblNewLabel.setBounds(135, 3, 229, 27);
		contentPane.add(lblNewLabel);
		
		JButton btnNewButton = new JButton("COFFEE");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Pmenu coffe1 = new Pmenu();
				coffe1.setVisible(true);
				dispose();
			}
		});
		btnNewButton.setFont(new Font("Trebuchet MS", Font.BOLD, 13));
		btnNewButton.setBounds(63, 41, 152, 34);
		contentPane.add(btnNewButton);
		
		JButton btnTea = new JButton("TEA");
		btnTea.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Mmenu tea1 = new Mmenu();
				tea1.setVisible(true);
				dispose();
			}
		});
		btnTea.setFont(new Font("Trebuchet MS", Font.BOLD, 13));
		btnTea.setBounds(293, 41, 152, 34);
		contentPane.add(btnTea);
		
		JButton btnNewButton_1 = new JButton("Cancel");
		btnNewButton_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Rece r = new Rece();
				r.setVisible(true);
				dispose();
			}
		});
		btnNewButton_1.setBounds(205, 100, 89, 39);
		contentPane.add(btnNewButton_1);
	}

}
